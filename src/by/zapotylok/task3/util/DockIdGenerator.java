package by.zapotylok.task3.util;

import java.util.concurrent.atomic.AtomicInteger;

public class DockIdGenerator {
	private static AtomicInteger dockId = new AtomicInteger();

	public static int generateDockId() {
		return dockId.incrementAndGet();
	}

}
