package by.zapotylok.task3.util;

import java.util.concurrent.atomic.AtomicInteger;

public class ShipIdGenerator {
	private static AtomicInteger shipId = new AtomicInteger();

	public static int generateShipId() {
		return shipId.incrementAndGet();
	}

}
