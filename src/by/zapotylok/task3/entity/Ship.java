package by.zapotylok.task3.entity;

import java.util.Random;

import by.zapotylok.task3.util.ShipIdGenerator;

public class Ship extends Thread {

	private int shipId;
	private int shipCapacity;
	private boolean load;
	private DockPool dockPool;

	public Ship(DockPool dockPool) {
		this.dockPool = dockPool;
		shipId = ShipIdGenerator.generateShipId();
		shipCapacity = new Random().nextInt(100);
		load = new Random().nextBoolean();

	}

	public int getShipCapacity() {
		return shipCapacity;
	}

	public int getShipId() {
		return shipId;
	}

	public boolean isLoad() {
		return load;

	}

	public void setLoad(boolean load) {
		this.load = load;
	}

	@Override
	public void run() {
		Dock dock = null;
		try {
			dock = dockPool.getDock();
			if (dock != null) {
				dock.reloadShip(this);
			}
		} finally {
			if (dock != null) {
				dockPool.putDock(dock);
			}
		}

	}

}
