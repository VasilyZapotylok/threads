package by.zapotylok.task3.entity;

import java.util.concurrent.locks.*;

import by.zapotylok.task3.util.DockIdGenerator;

public class Dock {

	private int dockId;
	private ReentrantLock lock = new ReentrantLock();

	public Dock() {
		dockId = DockIdGenerator.generateDockId();
	}

	public int getDockId() {
		return dockId;
	}

	public void reloadShip(Ship ship) {

		lock.lock();
		try {
			Storage storage = Storage.getStorage();
			if (ship.isLoad()) {
				storage.addCargoToStorage(ship);
			} else if (!ship.isLoad()) {
				storage.removeCargoFromStorage(ship);
			}
		} finally {
			lock.unlock();
		}
	}
}
