package by.zapotylok.task3.entity;

import java.util.Random;

import org.apache.log4j.Logger;

public class Storage {
	static final Logger LOGGER = Logger.getLogger(Storage.class);
	private static final int VOLUME = 10000;
	private int storageCapacity;
	private static Storage storage;

	public Storage() {

		storageCapacity = new Random().nextInt(1000);

	}

	public void addCargoToStorage(Ship ship) {

		int freeSpace = VOLUME - storageCapacity;

		if (ship.getShipCapacity() < freeSpace) {
			storageCapacity += ship.getShipCapacity();
			ship.setLoad(false);
			LOGGER.info("Ship id=" + ship.getShipId() + "is empty");
		} else {
			LOGGER.info("Ship id=" + ship.getShipId() + "can not reload");
		}

	}

	public void removeCargoFromStorage(Ship ship) {

		if (ship.getShipCapacity() < storageCapacity) {
			storageCapacity -= ship.getShipCapacity();
			ship.setLoad(true);
			LOGGER.info("Ship id=" + ship.getShipId() + "is full");
		} else {
			LOGGER.info("Ship id=" + ship.getShipId() + "can not reload");
		}

	}

	public static Storage getStorage() {
		if (storage == null) {
			storage = new Storage();
		}
		return storage;
	}
}
