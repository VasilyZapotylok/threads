package by.zapotylok.task3.entity;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

public class DockPool {
	static final Logger LOGGER = Logger.getLogger(DockPool.class);
	private final static int DOCK_POOL_SIZE = 3;
	private static AtomicBoolean isNull = new AtomicBoolean(true);
	private static ReentrantLock lock = new ReentrantLock();
	private static DockPool instance;
	private Semaphore semaphore = new Semaphore(DOCK_POOL_SIZE);
	private Queue<Dock> queue;

	public DockPool() {

		queue = new LinkedList<Dock>();
		for (int i = 0; i < DOCK_POOL_SIZE; i++) {
			queue.add(new Dock());
		}

	}

	public static DockPool getInstance() {
		if (isNull.get()) {
			lock.lock();
			try {
				if (isNull.get()) {
					instance = new DockPool();
					isNull.set(false);
				}
			} finally {
				lock.unlock();
			}
		}
		return instance;
	}

	public Dock getDock() {
		Dock dock = null;
		try {
			semaphore.acquire();
			dock = queue.poll();
		} catch (InterruptedException e) {
			LOGGER.error("Throuble with resourses");
		}
		return dock;
	}

	public void putDock(Dock dock) {
		queue.offer(dock);
		semaphore.release();
	}
}
